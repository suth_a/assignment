import { mount } from '@vue/test-utils'
import bg from '@/components/bg.vue'

describe('BG', () => {
	test('is a Vue instance', () => {
		const wrapper = mount(bg)
		expect(wrapper.isVueInstance()).toBeTruthy()
	})
	test('renders correctly', () => {
		const wrapper = mount(bg)
		expect(wrapper.element).toMatchSnapshot()
	})
})
