import Vuex from 'vuex'
import { createLocalVue, shallowMount } from '@vue/test-utils'

import players from '@/components/players'

const localVue = createLocalVue()
localVue.use(Vuex)

describe.only('Players', () => {
	let actions
	let store

	beforeEach(() => {
		actions = {
			getSrc: jest.fn()
		}
		store = new Vuex.Store({
			state: {},
			actions
		})
	})

	it('should dispatch action when created', () => {
		shallowMount(players, {
			localVue,
			store
		})
		expect(actions.getSrc).toHaveBeenCalled()
	})
})
