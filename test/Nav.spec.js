import { mount, createLocalVue } from '@vue/test-utils'
import navi from '@/components/navi.vue'

const localVue = createLocalVue()

localVue.component('nuxt-link', {
	props: ['to'],
	template: '<a href="#"><slot>NuxtLink</slot></a>'
})

describe('Test Nav', () => {
	const wrapper = mount(navi, {
		stubs: ['nuxt-link'],
		localVue
	})
	test('is a Vue instance', () => {
		expect(wrapper.isVueInstance()).toBeTruthy()
	})
})
