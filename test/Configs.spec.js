import { shallowMount } from '@vue/test-utils'
import players from '@/components/players.vue'

const factory = () => {
	return shallowMount(players, {})
}

describe('Players', () => {
	test('mounts properly', () => {
		const wrapper = factory()
		expect(wrapper.isVueInstance()).toBeTruthy()
	})

	test('renders properly', () => {
		const wrapper = factory()
		expect(wrapper.html()).toMatchSnapshot()
	})
})
