export const state = () => ({
	gtype: 'Competitive',
	mode: 'Squad Battles',
	status: 'Invite Only',
	queueLength: 4021,
	invitedPlayer: {}
})

export const mutations = {
	selectType(state, selected) {
		state.gtype = selected
	},
	selectMode(state, selected) {
		state.mode = selected
	},
	selectStatus(state, selected) {
		state.status = selected
	},
	decrementQueue(state) {
		state.queueLength = +state.queueLength - 114
	},
	incrementQueue(state) {
		state.queueLength = +state.queueLength + 188
	},
	playerInvited(state, player) {
		state.invitedPlayer = player
	}
}
